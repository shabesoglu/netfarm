# Netfarm is a[n attempt at creating a] replicated object system.

> "At this point it is a nonsense name."
> No? I think it does [make sense].
> It's a farm of objects in the net. I think that fits.

— Gilbert Baumann (`gilberth`)

## Introduction

Netfarm is our attempt at creating yet another distributed internet thingamabob.
It uses a distributed hash table, digital signatures and a portable bytecode
interpreter to allow objects to be replicated and intermingled, without possibly
being tampered with, regardless of implementation or application details.

We intend to improve on other internet thingamabobs by:

- using identities and data that aren't tied to servers or domains. Netfarm 
  uses a hash of a user information object, which includes their public keys, 
  as a universal identifier for a user,
- storing objects across a distributed hash-table to reduce the load on
  each node, and using the Kademlia algorithms to provide simple and efficient
  object retrieval,
- requiring signatures for changes to mutable objects, which are incredibly
  unlikely to be forgeable by anyone without access to identities' private
  keys,
- implementing a web-of-trust and collaborative-filtering-like system for 
  generalised user "voting" and other collective decisions about objects, and 
  only purging objects from nodes in extreme cases,
- recreating parts of an object system to provide schemas for objects and
  verification of them against those schemas, with a substantial (but admittedly
  not large) type system including collections, large integers, byte arrays and
  references to other objects, and
- providing reproducible side effects and scripting using a bytecode interpreter
  and separate computed values

For more details on why Netfarm may be more useful than other thingamabobs,
read the introduction of
[the Netfarm book](https://cal-coop.gitlab.io/netfarm/documentation/).

Feel free to drop into `#netfarm` on Freenode or `#netfarm:matrix.org` if you 
have any questions.

## Installing

1. **Install Quicklisp.** Netfarm requires a decent amount of libraries, and
   chasing them yourself isn't very fun or doable without a system manager
   (asdf) and system source (Quicklisp). You can download
   [the setup file](https://beta.quicklisp.org/quicklisp.lisp) and follow
   [the instructions for installation](https://www.quicklisp.org/beta/). It's
   very simple to install (just type some commands in a REPL)
   and requires no configuration.
2. **Download Netfarm.** Similarly, you need to put Netfarm somewhere Quicklisp
   looks. Hopefully you have the URL since either you're looking at the README
   there, or you've downloaded this library already.
3. If you want to test out networking, **Download cl-decentralise2.**
   [decentralise2](https://gitlab.com/cal-coop/netfarm/cl-decentralise2)
   is a library we wrote to handle object synchronisation and things like that 
   so that the Netfarm library could be mostly independent of network code, and
   to ensure that the interactions between Netfarm and decentralise2 code are
   minimal. Then **download netfarm-networking.** 
   [netfarm-networking](https://gitlab.com/cal-coop/netfarm/netfarm-networking)
   glues decentralise2 and Netfarm together, as to not clutter the object-system
   code repository.

## Examples

- Write out a random Lisp object to its Netfarm form then parse it back into 
  Lisp data
  We can take lists, strings, integers, byte vectors and booleans (encoded as
  `:true` or `:false`).

```lisp
CL-USER> (netfarm:render 42)
"integer:42"
CL-USER> (netfarm:parse *)
42
```

- Create a class and render an instance of it:
  As mentioned previously, this implementation of Netfarm hooks into the 
  meta-object protocol by providing a metaclass `netfarm:netfarm-class`. A hash 
  of the class will be used to identify the schema, as will every Netfarm 
  object.

```lisp
CL-USER> (defclass foo-class ()
           ((a :initarg :a)
            (b :initarg :b)
            (c :initarg :c))
           (:metaclass netfarm:netfarm-class))
           
#<NETFARM:NETFARM-CLASS COMMON-LISP-USER::FOO-CLASS>
CL-USER> (netfarm:render-object (make-instance 'foo-class :a "This is A" :b '("This" "is" 2) :c #(1 2 3 4 5)))
"---
\"schema\" \"JKSolr+at1/vu7B0UqWjenSIYwtvZFuTqwJW+GsPVJM=\"
---
\"This is A\"
(\"This\" \"is\" integer:2)
base64-data:AQIDBAU=
"
```

- Parse it back in:

```lisp
CL-USER> (netfarm:parse-block *)
#<NETFARM:VAGUE-OBJECT :SOURCE NIL
                       :SIGNATURES NIL
                       :METADATA #<HASH-TABLE :TEST EQUAL :COUNT 1 {100DDA19E3}>
                       :VALUES #<HASH-TABLE :TEST EQUAL :COUNT 3 {100DDA21A3}>
                       :NAME NIL
                       :COMPUTED-VALUES #<HASH-TABLE :TEST EQUAL :COUNT 0 {100DDA3BF3}>
                        {100DE72AD3}>
CL-USER> (netfarm:apply-class * (find-class 'foo-class))
#<FOO-CLASS {100DF985E3}>
CL-USER> (describe *)
#<FOO-CLASS {100DF985E3}>
  [standard-object]

Slots with :INSTANCE allocation:
  A                              = "This is A"
  B                              = ("This" "is" 2)
  C                              = #(1 2 3 4 5)
  SIGNATURES                     = NIL
  METADATA                       = #<HASH-TABLE :TEST EQUAL :COUNT 1 {100DDA19E3}>
  COMPUTED-VALUES                = #<HASH-TABLE :TEST EQUAL :COUNT 0 {100DDA3BF3}>
  SOURCE                         = NIL
  NAME                           = NIL
```

You can also experiment with the server and client using a
"passing connection" from decentralise2, after cloning the networking code:

```lisp
CL-USER> (ql:quickload '(:netfarm-client :netfarm-server))
To load "netfarm-client":
  ...
To load "netfarm-server":
  ...
CL-USER> (defvar *system*
           (make-instance 'netfarm-server:memory-system))
CL-USER> (decentralise-system:start-system *system*)
CL-USER> (defvar *client*
           (decentralise-client:connect-to-system *system*))
CL-USER> (make-instance 'foo-class 
                        :a "Hi there"
                        :c (make-instance 'foo-class
                                          :b "How are you?"
                                          :c (make-instance 'foo-class
                                                            :a "I'm doing fine")))

#<FOO-CLASS {1005F976D3}>
CL-USER> (netfarm-client:save-object *client* *)
"GCcy+vCHnFWJDI+U4MdDuLZp2zwHNgZIbzK4abRfbAs="
CL-USER> (netfarm-client:find-object *client* *)
#<FOO-CLASS {10043FF883}>
CL-USER> (slot-value * 'a)
"Hi there"
CL-USER> (slot-value ** 'c)
#<FOO-CLASS {10043FF4C3}>
CL-USER> (slot-value (slot-value * 'c) 'a)
"I'm doing fine"
```

There are also functions for generating keys, signing objects with said keys
and verifying them, as well as a script machine for handling side effects, but
those are boring without a friend to key around with, and slightly more complex
than what a README can handle, respectively.

## Todo

In no particular order:

- Devise a nice way for users to interact with objects.
- Document all the semantics of a Netfarm system.
- Write a clichéd joke about "world domination" or something.
- Write a dynamic compiler for Netfarm scripts, if interpreting them is
  too slow for some reasonable use.
