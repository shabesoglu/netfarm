(in-package :netfarm-tests)

(defclass noisy-interactive (interactive plain)
  ())

(define-test netfarm-tests
  :serial nil)

(defun run-tests (&key (interactive? nil)
                       (designators '(netfarm-tests benchmark)))
  (eql (status
	(test designators
              :report (if interactive? 'noisy-interactive 'plain)))
       :passed))

(defun gitlab-ci-test ()
  (or (run-tests)
      (uiop:quit -1)))
