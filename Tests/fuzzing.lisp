(in-package :netfarm-tests)

(defun test-fuzz-functions (a->b a &key b->a (test #'equal))
  (let ((b (funcall a->b a)))
    (unless (and (not (null b->a))
                 (funcall test (funcall b->a b) a))
      (error "could not backtrack to A"))
    b))

;; The tab and newline here are intentional. Please don't touch them.
(defvar *chars* "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890🆑🅾🆘👌 :.,	
カエルは有毒植物によって殴られます。")
(defun random-character-generator (&key
                                     (length 32)
                                     (chars *chars*))
  (let ((chars-length (length chars)))
    (flet ((random-character ()
             (char chars (random chars-length))))
      (lambda ()
        (loop with str = (make-string length)
              for pos below length
              do (setf (char str pos)
                       (random-character))
              finally (return str))))))

(defun random-vector-generator (&key
                                  (length 128))
  (lambda ()
    (loop
       with vector = (make-array length :element-type '(unsigned-byte 8))
       for n below length
       do (setf (aref vector n) (random 256))
       finally (return vector))))

(defvar *tree-depth* *reader-depth*)

(defun random-tree-generator (&key
                                (node-generator (random-datum-generator))
                                (branch-probability 0.5)
                                (length 5))
  (lambda ()
    (labels ((recur ()
               (if (zerop *tree-depth*)
                   (funcall node-generator)
                   (let ((*tree-depth* (1- *tree-depth*)))
                     (loop repeat (random length)
                           collect (if (< (random 1.0) branch-probability)
                                       (recur)
                                       (funcall node-generator)))))))
      (recur))))

(defun random-datum-generator (&key
                                 (string-length 64)
                                 (vector-length 64)
                                 (integer-size #.(expt 2 32)))
  (let ((string-gen (random-character-generator :length string-length))
        (vector-gen (random-vector-generator :length vector-length))
        (number-gen (lambda () (random integer-size))))
    (lambda ()
      (case (random 4)
        (0 (funcall string-gen))
        (1 (funcall vector-gen))
        (2 (funcall number-gen))
        (3 (if (zerop (random 2))
               :true
               :false))))))

(defclass fuzz-class ()
  ((slot-a :initarg :slot-a)
   (slot-b :initarg :slot-b))
  (:metaclass netfarm-class))
(defun random-object-generator (&key
                                  (slot-generator (random-datum-generator)))
  (lambda ()
    (make-instance 'fuzz-class
                   :slot-a (funcall slot-generator)
                   :slot-b (funcall slot-generator))))

(defgeneric equal-dwim (a b)
  (:method (a b) nil)
  (:method ((a string) (b string))
    (string= a b))
  (:method ((a number) (b number))
    (= a b))
  (:method ((a vector) (b vector))
    (and (= (length a) (length b))
         (every #'= a b)))
  (:method ((a list) (b list))
    (and (= (length a) (length b))
         (every #'equal-dwim a b)))
  (:method ((a symbol) (b symbol))
    (eql a b)))

(defun fuzz (fn tests input-generator &key reverse-fn (test #'equal))
  (let ((errors 0)
        (error-cases nil))
    (dotimes (test-n tests)
      (let ((input (funcall input-generator)))
        (handler-case
            (test-fuzz-functions fn input :b->a reverse-fn :test test)
          (condition (c)
            (incf errors)
            (push input error-cases)
            (format t "BANG! The input ~s raises the condition ~a on test #~d.~%"
                    input c test-n)
            (when (> errors 100)
              (return-from fuzz
                           (values nil errors error-cases)))))))
    (values (zerop errors)
            errors
            error-cases)))
