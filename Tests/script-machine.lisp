(in-package :netfarm-tests)

(define-test script-machine-tests
  :parent netfarm-tests)

(netfarm-scripts:define-script *two-plus-two* ()
  (:procedure 0) (byte 2) dup + return)

(netfarm-scripts:define-script *also-fairly-basic-maths* ()
  (:procedure 0) (byte 2) (byte 3) < return)

(defmacro should-return ((script-name &rest arguments) &rest values)
  `(is equal-dwim 
       ',values
       (netfarm-scripts:run-interpreter
        (netfarm-scripts:setup-interpreter ,script-name (list ,@arguments)))))

(defmacro method-should-return ((object method-name &rest arguments) &rest values)
  `(is equal-dwim
       ',values
       (netfarm-scripts:run-interpreter
        (netfarm-scripts:setup-method-interpreter ,object ,method-name (list ,@arguments)))))

(define-test quick-maths
  :parent script-machine-tests
  (should-return (*two-plus-two*) 4)
  (should-return (*also-fairly-basic-maths*) :true))

(netfarm-scripts:define-script *get-the-foo* ("foo")
  (:method "method" 0)
  self (get-value 0) object-value return)

(defclass thing-with-foo ()
  ((foo :initarg :foo))
  (:metaclass netfarm:netfarm-class)
  (:scripts *get-the-foo*))

(netfarm-scripts:define-script *call-method* ("method")
  (:procedure 1)
  (get-env 0 0) (get-value 0) (call-method 0) return)

(define-test method-call
  :parent script-machine-tests
  (should-return (*call-method* (make-instance 'thing-with-foo :foo "This is a foo"))
                 ("This is a foo"))
  (should-return (*call-method* (make-instance 'thing-with-foo))
                 :false)
  (method-should-return ((make-instance 'thing-with-foo :foo "This is a foo") "method")
                        "This is a foo")
  (fail (netfarm-scripts:run-interpreter
         (netfarm-scripts:setup-method-interpreter (make-instance 'thing-with-foo)
                                                   "method"
                                                   '()))
      error "The slot FOO was unbound, so taking OBJECT-VALUE should have unwound."))

(netfarm-scripts:define-script *loop-forever* ()
  (:procedure 0)
  (get-proc* 0) (tail-call 0))

(define-test cycle-limit
  :parent script-machine-tests
  (fail (netfarm-scripts:run-interpreter
         (netfarm-scripts:setup-interpreter *loop-forever* '())
         :cycle-limit 10000)
      error "We set a cycle limit, and it should have been exceeded."))
