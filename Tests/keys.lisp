(in-package :netfarm-tests)

(defclass song ()
  ((lyrics :initarg :lyrics :reader song-lyrics))
  (:metaclass netfarm-class))

(defvar *song-about-stuff* (make-instance 'song :lyrics "Photographers snip snap
Take your time she's only burning
This kind of experience
Is necessary for her learning"))

(define-test key-tests :parent netfarm-tests)

(define-test symmetric
  :parent key-tests
  (let* ((key1 (generate-keys))
         (key2 (generate-keys))
         (ecdh1 (shared-key key1 key2))
         (ecdh2 (shared-key key2 key1))
         ;; Ciphers do have state, don't reuse them.
         (cipher1 (symmetric-cipher ecdh1))
         (cipher2 (symmetric-cipher ecdh2)))
    (true (every #'= ecdh1 ecdh2) "A common ECDH key was not created.")
    (is string= (song-lyrics *song-about-stuff*)
	(symmetric-decrypt cipher1
			   (symmetric-encrypt cipher2
                                              (song-lyrics *song-about-stuff*)))
        "A string could not be reliably encrypted and decrypted")))

(define-test verifier
  :parent key-tests
  (let* ((digest       (hash-object *song-about-stuff*))
         (verifier     (readable-verifier digest))
         (bad-digest   (hash-object
                        (make-instance 'song
                         :lyrics (substitute #\o #\e
                                             (song-lyrics *song-about-stuff*)))))
         (bad-verifier (readable-verifier bad-digest)))
    (isnt string= verifier bad-verifier
          "The mutated string and the original string created the same verifier")))

(defclass random-object ()
  ()
  (:metaclass netfarm-class))

(define-test sign-object
  :parent key-tests
  (let* ((object     (make-instance 'song
                      :lyrics (song-lyrics *song-about-stuff*)))
         (keys       (generate-keys))
         (user       (keys->object keys)))
    (add-signature object keys user)
    (true (verify-object-signatures object)
	  "Could not verify a signed object")))
