(asdf:defsystem :netfarm-tests
  :depends-on (:netfarm :parachute :the-cost-of-nothing :uiop)
  :serial t
  :components ((:file "package")
               (:file "run-tests")
               (:file "fuzzing")
               (:file "keys")
               (:file "new-parser")
               (:file "new-renderer")
               (:file "script-machine")
               (:file "objects")
               (:file "inbuilt-schemas")
               (:file "benchmark")))
