(in-package :netfarm-tests)

(define-test objects :parent netfarm-tests)

(define-test make-class
  :parent objects
  (let* ((schema (make-instance 'schema
                                :slots '(("foo") ("bar"))
                                :computed-slots '()
                                :scripts '()
                                :documentation ""))
         (class  (schema->class schema))
         (class2 (schema->class schema))
         (instance (make-instance class)))
    (true (typep instance class))
    (true (typep instance 'object))
    (is eq class class2 "schema->class should be hash consed")
    (is = 0 (netfarm-class-slot-position class "foo")
        "The positions of slots in an external class shouldn't change")))

(defclass class1 ()
  ()
  (:metaclass netfarm-class))
(defclass class2 (class1)
  ()
  (:metaclass netfarm-class))

(defgeneric dispatch-ordering-test-function (object)
  (:method-combination list)
  (:method list ((object class1)) 'class1)
  (:method list ((object object)) 'object)
  (:method list ((object class2)) 'class2))

;; Check that methods are dispatched the right way, i.e. that methods
;; specialised on an OBJECT are selected after methods specialised on
;; some other NETFARM-CLASS.
(define-test dispatch
  :parent objects
  (is equal '(class1 object)
      (dispatch-ordering-test-function (make-instance 'class1)))
  (is equal '(class2 class1 object)
      (dispatch-ordering-test-function (make-instance 'class2))))

(define-test slots
  :parent objects
  (fail (defclass bogus-class1 ()
          ((slot-a :netfarm-name "foo")
           (slot-b :netfarm-name "foo"))
          (:metaclass netfarm:netfarm-class))
      error "Two slots cannot have the same Netfarm name")
  (finish (defclass okay-class1 ()
            ((slot))
            (:metaclass netfarm:netfarm-class)))
  (fail (defclass bogus-class2 (okay-class1)
          ((slot :netfarm-name "not-a-slot"))
          (:metaclass netfarm:netfarm-class))
      error "A slot cannot have two Netfarm names")
  (finish (defclass okay-class2 ()
            ((slot :netfarm-name nil))
            (:metaclass netfarm:netfarm-class)))
  (fail (defclass bogus-class3 (okay-class2)
          ((slot :netfarm-name "a-slot"))
          (:metaclass netfarm:netfarm-class))
      error "A slot cannot have no name and have a name")
  (fail (defclass bogus-class4 (okay-class2)
          ((slot :netfarm-name "a-slot" :computed t))
          (:metaclass netfarm:netfarm-class))
      error "A slot cannot be computed and not computed simultaneously"))
