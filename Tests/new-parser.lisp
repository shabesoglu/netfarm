(in-package :netfarm-tests)

(define-test parser-tests :parent netfarm-tests)

(defmacro with-parsed ((block &key (text (gensym)) (vague (gensym))
                                   class (object (gensym)))
                       &body body)
  `(let* ((,text ,block)
          (,vague (parse-block ,text
                               :source (lambda (hash)
                                         (make-instance 'netfarm:reference
                                                        :hash hash))))
          (,object ,(if (null class)
                        nil
                        `(apply-class ,vague
                                      (find-class ',class)))))
     (declare (ignorable ,text ,vague ,object))
     ,@body))

(define-test parse-atoms
  :parent parser-tests
  (is string= "Hello world!"
      (parse "base64:SGVsbG8gd29ybGQh"))
  (is equal-dwim #(1 2 3) (parse "base64-data:AQID"))
  (fail (parse "base64-data:AAAAAA")))

(define-test parse-lists
  :parent parser-tests
  (is equal-dwim '("A" "a letter" 123 (4 "five"))
      (parse "(A \"a letter\" integer:123 (integer:4 five))"))
  (let ((*reader-depth* 1))
    (is equal '() (parse "()"))
    (fail (parse "(())"))))

(define-test fuzz-test
  :parent parser-tests
  ;; Do some fuzz testing.
  (true (fuzz #'render 10000 (random-tree-generator)
	      :reverse-fn #'parse
	      :test #'equal-dwim)
	"Fuzz testing the renderer and parser failed, see the 'BANG' messages above")
  (let ((fuzz-class (find-class 'fuzz-class)))
    (true (fuzz #'render-object 10000 (random-object-generator)
                :reverse-fn (lambda (text)
                              (apply-class (parse-block text)
                                           fuzz-class))
                :test (lambda (object1 object2)
                        (and (equal-dwim (slot-value object1 'slot-a)
                                         (slot-value object2 'slot-a))
                             (equal-dwim (slot-value object1 'slot-b)
                                         (slot-value object2 'slot-b))))))))
                      

(define-test read-signature
  :parent parser-tests
  (with-parsed ("AQIDBAUGBwgJCgsMDQ4PEBESExQVFhcYGRobHB0eHyA= Bw8XHycvNz9HT1dfZ293f4ePl5+nr7e/x8/X3+fv9/8HDxcfJy83P0dPV19nb3d/h4+Xn6evt7/Hz9ff5+/3/w==
Bw8XHycvNz9HT1dfZ293f4ePl5+nr7e/x8/X3+fv9/8= AQIDBAUGBwgJCgsMDQ4PEBESExQVFhcYGRobHB0eHyABAgMEBQYHCAkKCwwNDg8QERITFBUWFxgZGhscHR4fIA==
---
---
"
                :vague vague-object)
    (let* ((signature-pairs (vague-object-signatures vague-object))
           (users           (mapcar (alexandria:compose #'reference-hash #'car) signature-pairs))
           (signatures      (mapcar #'cdr signature-pairs)))
      (is equalp '(#( 1  2  3  4  5  6  7  8
		     9 10 11 12 13 14 15 16
		     17 18 19 20 21 22 23 24
		     25 26 27 28 29 30 31 32)
                   #(  7  15  23  31  39  47  55  63
                      71  79  87  95 103 111 119 127
                     135 143 151 159 167 175 183 191
                     199 207 215 223 231 239 247 255))
          users)
      (is equalp '(#(  7  15  23  31  39  47  55  63
                      71  79  87  95 103 111 119 127
                     135 143 151 159 167 175 183 191
                     199 207 215 223 231 239 247 255
                       7  15  23  31  39  47  55  63
                      71  79  87  95 103 111 119 127
                     135 143 151 159 167 175 183 191
                     199 207 215 223 231 239 247 255)
                   #( 1  2  3  4  5  6  7  8
                      9 10 11 12 13 14 15 16
                     17 18 19 20 21 22 23 24
                     25 26 27 28 29 30 31 32
                      1  2  3  4  5  6  7  8
                      9 10 11 12 13 14 15 16
                     17 18 19 20 21 22 23 24
                     25 26 27 28 29 30 31 32))
          signatures))))

(defclass foo-bar ()
  ((foo :reader foo-bar-foo)
   (bar :computed t :reader foo-bar-bars))
  (:metaclass netfarm:netfarm-class))

(define-test parse-object
  :parent parser-tests
  (with-parsed ("---
\"schema\" \"riZGvNLSL8nMmp4Mma4sOW9ZAlLz1Tr1DK3qaNlm6J8=\"
---
\"this is the foo\"
---
((ref:xBRN+HW8oqKzKiSne8sxuxdTVrN/h0MBhGEMxVvGDP4= \"this is the bar\"))"
                :vague vague-object
                :class foo-bar
                :object object)
    (let ((vague-values (vague-object-values vague-object)))
      (is string= "this is the foo" (aref vague-values 0)))
    (is string= "this is the foo" (foo-bar-foo object))
    (let ((computed-value (first (foo-bar-bars object))))
      (is string= "this is the bar" (second computed-value)))))
      
(defun one (x)
  (declare (ignore x))
  1)

(defclass juan-class ()
  ((always-one :initform 4
               :renderer one
               :parser   one
               :reader juan))
  (:metaclass netfarm:netfarm-class)
  (:documentation "A class which should always render and parse the ALWAYS-ONE slot as 1"))

(define-test renderer-parser
  :parent parser-tests
  (with-parsed ((netfarm:render-object (make-instance 'juan-class))
                :class juan-class
                :object object)
    (is = 1 (juan object)))
  (with-parsed ("---
\"schema\" \"1ihepCk+7FsCJ0IhIvlWgmANHUepSEOAf23ADZlzC1o=\"
---
integer:42"
                :class juan-class
                :object object)
    (is = 1 (juan object))))
