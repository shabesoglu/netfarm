(in-package :netfarm-tests)

(define-test benchmark)

(defclass benchmark-class ()
  ((foo :initarg :foo :accessor foo)
   (bar :initarg :bar)
   (baz :initarg :baz)
   (quux :initarg :quux))
  (:metaclass netfarm-class))

(defmacro announce-bench-group (name)
  `(format *trace-output* "~&Benchmarking ~a:" ,(format nil "~(~a~)" name)))
(defmacro named-bench (name form)
  `(progn
     (format *trace-output* "~&| ~24a " ,(format nil "~(~a~)" name))
     (the-cost-of-nothing:bench ,form)))
(defmacro named-bench/mips (name script-name &rest arguments)
  (let ((run-script `(netfarm-scripts:run-interpreter
                      (netfarm-scripts:setup-interpreter ,script-name (list ,@arguments)))))
    `(progn
       (format *trace-output* "~&| ~24a " ,(string-downcase name))
       (multiple-value-bind (results interpreter)
           ,run-script
         (declare (ignore results))
         (let* ((setup-time (the-cost-of-nothing:benchmark
                             (netfarm-scripts:setup-interpreter ,script-name
                                                                (list ,@arguments))))
                (operations (netfarm-scripts:interpreter-instruction-count interpreter))
                (time (- (/ (the-cost-of-nothing:benchmark (dotimes (n 15) ,run-script))
                            15.0)
                         setup-time)))
           (the-cost-of-nothing:write-si-unit (float (/ operations time))
                                              "operations/second"
                                              *trace-output*)
           (write-string " (" *trace-output*)
           (the-cost-of-nothing:write-si-unit time "seconds" *trace-output*)
           (write-string ")" *trace-output*))))))
(defmacro named-bench/bytes (name object form)
  `(progn
     (format *trace-output* "~&| ~24a" ,(string-downcase name))
     (let* ((byte-length (length (netfarm:binary-render-object ,object)))
            (time (the-cost-of-nothing:benchmark ,form)))
       (the-cost-of-nothing:write-si-unit (float (/ byte-length time))
                                          "bytes/second"
                                          *trace-output*))))
     
     
(define-test object-value-benchmark
  :parent benchmark
  (let ((instance (make-instance 'benchmark-class
                                 :foo "foo"
                                 :bar "bar"
                                 :baz "baz")))
    (announce-bench-group readers)
    (named-bench accessor     (foo instance))
    (named-bench slot-value   (slot-value instance 'bar))
    (named-bench object-value (object-value instance "baz"))
    (announce-bench-group writers)
    (named-bench (setf accessor) (setf (foo instance) "fooo"))
    (named-bench (setf slot-value) (setf (slot-value instance 'bar) "baar"))
    (named-bench (setf object-value) (setf (object-value instance "baz") "baaz"))))

(define-test render-benchmark
  :parent benchmark
  (let ((instance (make-instance 'benchmark-class
                                 :foo '("foo" "bar" :false)
                                 :bar #(1 2 3 4 5)
                                 :baz 123456789123456789))
        (tree '("define" ("fact" "x")
                 ("if" ("=" "x" 0)
                       1
	               ("*" ("fact" ("sub1" "x")) "x"))))
	;; This is a very good excuse to use easily the greatest part of Netfarm:
	;; the one and only testimage.png, with its pun that surpasses any
        ;; proggithumor meme in terms of funniness
	(blob (alexandria:read-file-into-byte-vector
	       (asdf:system-relative-pathname :netfarm-tests
					      #p"testimage.png"))))
    (announce-bench-group rendering)
    (with-hash-cache ()
      (let ((nowhere (make-broadcast-stream)))
        (named-bench render-object (render-object instance :stream nowhere))
        (named-bench render        (render tree nowhere))
        (named-bench render-blob   (render blob nowhere))
        (named-bench binary-render-object
                     (binary-render-object instance
                                           :function (constantly t)))
        (named-bench binary-render (binary-render tree (constantly t)))
        (named-bench binary-render-blob (binary-render blob (constantly t)))))))

;;; Script machine benchmarks

(netfarm-scripts:define-script *fib* ()
  (:procedure 1)
  ;; (if (< x 2) x (+ (fib (- x 1)) (fib (- x 2))))
  (get-env 0 0) (byte 2) <
  (jump-cond 0 0 0 4)
  (get-env 0 0) return
  (get-env 0 0) (byte 1) - (get-proc* 0) (call 1)
  (get-env 0 0) (byte 2) - (get-proc* 0) (call 1)
  + return)

(netfarm-scripts:define-script *loop* ()
  (:procedure 1)
  (get-env 0 0) (byte 0) =
  (jump-cond 0 0 0 4)
  (get-env 0 0) return
  (get-env 0 0) (byte 1) - (get-proc* 0) (tail-call 1))

(netfarm-scripts:define-script *cons-loop* ("bloop")
  (:procedure 2)
  (get-env 1 0) (byte 0) =
  (jump-cond 0 0 0 4)
  (get-env 0 0) return
  (get-value 0) (get-env 0 0) cons
  (get-env 1 0) (byte 1) -
  (get-proc* 0) (tail-call 2))

(netfarm-scripts:define-script *sub1-method* ("change")
  (:method "sub1" 1)
  (get-env 1 0) self (get-value 0) object-value - return)
(defclass sub1-executor ()
  ((change :initform 1))
  (:documentation "If we see a class like this in the wild, we will be very disappointed in you.
We just want to test method dispatch.")
  (:scripts *sub1-method*)
  (:metaclass netfarm-class))

(netfarm-scripts:define-script *sub1-method-loop* ("sub1")
  (:procedure 2)
  (get-env 0 0) (byte 0) =
  (jump-cond 0 0 0 4) (get-env 0 0) return
  ;; Call the method to decrement the counter.
  (get-env 0 0) (get-env 1 0) (get-value 0) (call-method 1)
  ;; Unwrap the result.
  car
  ;; Repeat.
  (get-env 1 0) (get-proc* 0) (tail-call 2))

(define-test script-machine-benchmark
  :parent benchmark
  (netfarm-scripts::ensure-dispatch-function)
  (announce-bench-group script-machine)
  (named-bench/mips "fib(28)"   *fib*  28)
  (named-bench/mips "loop"      *loop* 2000000)
  (named-bench/mips "cons-loop" *cons-loop* '() 1000000)
  (named-bench/mips "method-call" *sub1-method-loop*
                    200000 (make-instance 'sub1-executor)))

(defvar *words* #("a" "administrators" "an" "and" "at" "attempt"
                  "bubble" "client" "creating" "decentralisation"
                  "develop" "devise" "distributed" "do" "fault" "fill"
                  "greater" "hash" "hierarchical" "in" "is"
                  "language's" "leave" "less" "meta" "moderation"
                  "netfarm" "not" "object" "of" "operators"
                  "perceived" "programming" "protocol"
                  "rapidly-expanding" "servers'" "simplify" "system"
                  "table" "techniques" "that" "the" "their" "them"
                  "to" "tolerance" "trustless" "utilise" "via" "void"
                  "we" "whim"))

(define-test crypto-benchmark
  :parent benchmark
  (announce-bench-group cryptographic-operations)
  (let* ((keys         (generate-keys))
         (user         (keys->object keys))
         (small-object (make-instance 'song :lyrics "Aaaaaaaaaaaaaaaaaaaaaaaaaaargh!"))
         (large-object (make-instance 'song
                        :lyrics (loop repeat 100000
                                      collect (alexandria:random-elt *words*)))))
    (named-bench/bytes hash-small small-object (hash-object small-object))
    (named-bench/bytes hash-large large-object (hash-object large-object))
    (named-bench/bytes sign-small small-object
                       (add-signature small-object keys user))
    (named-bench/bytes sign-large large-object
                       (add-signature large-object keys user))))
