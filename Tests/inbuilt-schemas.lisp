(in-package :netfarm-tests)

(define-test inbuilt-schemas :parent netfarm-tests)

(netfarm-scripts:define-script *clingy-script* ("hello" "add-datum")
  (:method "initialize" 0)
  ;; Construct the list ("hello" <self>)
  (get-value 0) self (list 2)
  ;; Get my (first) author
  self object-authors car
  ;; Send myself
  (get-value 1) (call-method 1) return)
(defclass clingy-object ()
  ()
  (:scripts *clingy-script*)
  (:metaclass netfarm:netfarm-class))

(netfarm-scripts:define-script *bogus-clingy-script* ("hello" "author" "add-datum")
  (:method "initialize" 0)
  ;; Construct the list ("hello" <self>)
  (get-value 0) self (list 2)
  ;; Get my "author"
  self (get-value 1) object-value
  ;; Send myself
  (get-value 2) (call-method 1) return)
(defclass bogus-clingy-object ()
  ((author :initarg :author))
  (:scripts *bogus-clingy-script*)
  (:metaclass netfarm:netfarm-class))

(defun set-equal (list1 list2 &key (key #'identity) (test #'equal))
  (null (set-exclusive-or list1 list2 :key key :test test)))

(define-test user-data-tests
  :parent inbuilt-schemas
  (let* ((keys (generate-keys))
         (user (keys->object keys))
         (object1 (make-instance 'clingy-object))
         (object2 (make-instance 'bogus-clingy-object :author user)))
    (add-signature object1 keys user)
    (netfarm-scripts:run-script-machines object1)
    (true (member object1 (user-values user "hello"))
        "OBJECT1 should have been added as a value, as it was signed by the user")
    (netfarm-scripts:run-script-machines object2)
    (false (member object2 (user-values user "hello"))
           "OBJECT2 should not have been added as a value, as it was not signed")))
