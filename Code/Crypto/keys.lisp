(in-package :netfarm)

;;; A base type for holding partial and full keys.

(defstruct (keys (:constructor make-keys))
  signature-public
  signature-private
  exchange-public
  exchange-private)

(defun generate-keys ()
  (multiple-value-bind (s-pri s-pub)
      (generate-key-pair :ed25519)
    (multiple-value-bind (e-pri e-pub)
        (generate-key-pair :curve25519)
      (make-keys :signature-public s-pub
                 :signature-private s-pri
                 :exchange-public e-pub
                 :exchange-private e-pri))))

(defun keys-from-public (signature exchange)
  (make-keys :signature-public signature
             :exchange-public exchange))

(defun object->keys (object)
  (make-keys :signature-public (make-public-key :ed25519
                                                :y (user-sign-key object))
             :exchange-public  (make-public-key :curve25519
                                                :y (user-ecdh-key object))))

(defun keys->object (keys)
  (make-instance 'user
                 :sign-key (ed25519-key-y (keys-signature-public keys))
                 :ecdh-key (curve25519-key-y (keys-exchange-public keys))))

;;; Elliptic Curve Diffie-Hellman helpers

(defun shared-key (ours theirs)
  (diffie-hellman
   (keys-exchange-private ours)
   (keys-exchange-public  theirs)))

(defun symmetric-cipher (key)
  (make-cipher :aes
               :mode :ctr
               :key key
               :initialization-vector
               (digest-sequence :ripemd-128 key)))

(defun symmetric-encrypt (key text)
  (encrypt-message key
                   (babel:string-to-octets text :encoding :utf-8)))

(defun symmetric-decrypt (key text)
  (babel:octets-to-string (decrypt-message key text)
                          :encoding :utf-8))

;;; Elliptic Curve Digital Signing Algorithm helpers

(declaim (inline sign-object verify-object-signature))

(defun verify-object-signature (key signature hash)
  (declare (keys key)
           ((simple-array (unsigned-byte 8) (*)) signature hash))
  (verify-signature (keys-signature-public key)
                    hash signature))

(defun sign-object (object key)
  (sign-message (keys-signature-private key)
                (base-hash-object object :emit-signatures nil)))

(defun verify-object-signatures (object)
  (when (endp (object-signatures object))
    (return-from verify-object-signatures t))
  (loop with hash = (base-hash-object object :emit-signatures nil)
        for (user . signature) in (object-signatures object)
        always (verify-object-signature (object->keys user)
                                        signature hash)))

(defun verify-vague-object-signatures (vague-object)
  (when (endp (vague-object-signatures vague-object))
    (return-from verify-vague-object-signatures t))
  (loop with hash = (base-hash-object vague-object :emit-signatures nil)
        for (user-reference . signature)
          in (vague-object-signatures vague-object)
        for user = (funcall (vague-object-source vague-object)
                            (reference-hash user-reference))
        always (verify-object-signature (object->keys user)
                                        signature hash)))

(defun add-signature (object key user)
  (push (cons user (sign-object object key))
        (object-signatures object))
  object)
