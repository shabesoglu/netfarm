(in-package :netfarm)

(defun decode-base64-string (string)
  (with-input-from-string (stream string)
    (decode-base64-bytes stream)))

(defun parse-signatures (lines)
  (loop for line in lines
        collect (destructuring-bind (owner* signature*) (split-sequence #\Space line)
                  (let ((owner     (decode-base64-string owner*))
                        (signature (decode-base64-string signature*)))
                    (assert (= (length owner) 32))
                    (assert (= (length signature) 64))
                    (cons (make-instance 'reference :hash owner) signature)))))

(defun parse-all (stream)
  "Parse every value in a stream into a list."
  (when (stringp stream)
    (setf stream (make-string-input-stream stream)))
  (loop until (null (peek-char t stream nil nil))
        collect (parse stream)))

(defvar *in-slot?* nil)
(defvar *reader-depth* 32)
(defvar *backslash-chars* '((#\n . #\Newline)
                            (#\" . #\")
                            (#\\ . #\\)))
(defun parse (stream &key (allow-codecs t))
  "Parse a Netfarm value from the stream or string STREAM. If ALLOW-CODECS is
changed to NIL, then a condition of type PARSER-CODECS-IN-KEY will be signalled
if the value contains calls to codecs or lists."
  (when (stringp stream)
    (setf stream (make-string-input-stream stream)))
  (case (peek-char t stream)
    (#\~
     (unless *in-slot?*
       (error "Can't have unbound slot while not in a slot"))
     :unbound)
    (#\(
     (unless allow-codecs
       (error 'parser-codecs-in-key :atom "("))
     (read-char stream)
     (assert (plusp *reader-depth*) () "too much ( recursion")
     (let ((*reader-depth* (1- *reader-depth*))
           (*in-slot?* nil))
       (loop until (char= (peek-char t stream) #\))
             collect (parse stream :allow-codecs allow-codecs)
             ;; Eat the )
             finally (read-char stream))))
    (#\)
     (error "unmatched )"))
    (#\"
     (read-char stream)
     (with-output-to-string (accumulator)
       (loop
         (when (char= (peek-char nil stream) #\")
           (return))
         (let ((char (read-char stream)))
           (if (char= char #\\)
               (let ((char (read-char stream)))
                 (write-char (or (cdr (assoc (char-downcase char) *backslash-chars*))
                                 char)
                             accumulator))
               (write-char char accumulator))))
       ;; Eat the last "
       (read-char stream)))
    (t (apply-atom-codecs
        (with-output-to-string (accumulator)
          (loop until (member (peek-char nil stream nil nil) '(nil #\Space #\)))
                do (write-char (read-char stream) accumulator)))
        :allow-codecs allow-codecs))))

(defun parse-hash-table (lines)
  (loop with value-table = (make-hash-table :test 'equal)
        for line in lines
        unless (or (zerop (length line))
                   (char= (char line 0) #\#))
          do (destructuring-bind (key value) (parse-all line)
               (assert (not (nth-value 1 (gethash key value-table))) ()
                       'netfarm-slot-duplicate-binding :slot-name key)
               (setf (gethash key value-table) value))
        finally (return value-table)))

(defun parse-vector (lines)
  (let ((vector (make-array (length lines))))
    (loop for line in lines
          for position from 0
          do (setf (aref vector position)
                   (destructuring-bind (value) (parse-all line)
                     value)))
    vector))

(defun parse-segments (segments &key source name)
  "Parse the Netfarm block in TEXT. If SOURCE is provided, it is the source of
the generated VAGUE-OBJECT."
  (unless (<= 3 (length segments) 4)
    (error 'parser-bad-segment-count
           :count (length segments)))
  (destructuring-bind (signatures metadata values
                       &optional computed-values)
      segments
    (make-instance 'vague-object
                   :name name
                   :source source
                   :signatures      (parse-signatures signatures)
                   :metadata        (parse-hash-table metadata)
                   :values          (let ((*in-slot?* t))
                                      (parse-vector values))
                   :computed-values (let ((*in-slot?* t))
                                      (parse-vector computed-values)))))

(defun split-segments (text)
  (when (stringp text)
    (setf text (split-sequence #\Newline text :remove-empty-subseqs t)))
  (split-sequence "---" text :test #'string=))

(defun parse-block (text &key source name)
  (parse-segments (split-segments text)
                  :name name
                  :source source))
