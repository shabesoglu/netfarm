(in-package :netfarm)

(defvar *codecs* (make-hash-table :test 'equal))
(defvar *encoding* :utf-8)

(defstruct codec
  encoder decoder)

(defmacro define-codec (name encode decode)
  (when (symbolp name)
    (setf name (string-downcase name)))
  `(setf (gethash ',name *codecs*)
         (make-codec :encoder ,encode
                     :decoder ,decode)))

(define-codec integer
  (lambda (number)
    (format nil "~d" number))
  #'parse-integer)

(define-codec boolean
  (lambda (boolean)
    (ecase boolean
      (:true "true")
      (:false "false")))
  (lambda (string)
    (cond
      ((string= string "true") :true)
      ((string= string "false") :false)
      (t (error 'type-error
                :context '(:codec boolean)
                :expected-type '(member :true :false)
                :datum string)))))

(define-codec base64
  (lambda (string)
    (bytes->base64 (string-to-octets string :external-format *encoding*)))
  (lambda (base64)
    (octets-to-string (base64->bytes base64)
                      :external-format *encoding*)))

(define-codec base64-data
  #'bytes->base64
  (lambda (string)
    (let* ((bytes* (base64->bytes string))
           (length (length bytes*)))
      (coerce (subseq bytes* 0 length) '(vector (unsigned-byte 8))))))

(define-codec ref
  (lambda (reference)
    (check-type reference reference)
    (reference-hash reference))
  (lambda (string)
    (make-instance 'reference :hash string)))

(defun find-codec (name type)
  (let ((codec-definition (gethash name *codecs*)))
    (when (null codec-definition)
      (error "no codec named ~a" name))
    (ecase type
      (:encode (codec-encoder codec-definition))
      (:decode (codec-decoder codec-definition)))))
  

(defun decode-codecs (codec-list data)
  (dolist (codec codec-list data)
    (setf data (funcall (find-codec codec :decode) data))))

(defun apply-atom-codecs (atom &key (allow-codecs t))
  (case (count #\: atom :test #'char=)
    (0 atom)
    (1 (destructuring-bind (codecs data)
           (split-sequence #\: atom)
         (if allow-codecs
             (decode-codecs (split-sequence #\, codecs) data)
             (error 'parser-codecs-in-key :atom atom))))
    (otherwise (error "Too many colons in ~a" atom))))

(defun render-string (string stream)
  (declare (string string))
  (write-char #\" stream)
  (loop for char across string
	do (case char
	     (#\Newline (write-string "\\n" stream))
             ;; writes \"
	     (#\" (write-string "\\\"" stream))
             ;; writes \\
	     (#\\ (write-string "\\\\" stream))
	     (t (write-char char stream))))
  (write-char #\" stream))
