(asdf:defsystem :netfarm-early
  :depends-on (:alexandria :babel :closer-mop :flexi-streams
               :ironclad :s-base64 :split-sequence
               :trivial-garbage :bordeaux-threads)
  :components ((:file "package")
               (:file "conditions" :depends-on ("package"))
               (:file "macros")
               (:module "Objects"
                :depends-on ("package" "macros")
                :serial t
                :components ((:file "hash")
                             ;; How to bootstrap Netfarm in 11 not easy steps:
                             ;; 1. Create the metaclass NETFARM-CLASS
                             (:module "MOP"
                              :components ((:file "consistent-state")
                                           (:file "netfarm-class")
                                           (:file "netfarm-slot")
                                           (:file "rewrite-references")
                                           (:file "patch-object-class")))
                             (:file "objects")
                             ;; 2. Set up schema and class tables
                             (:file "hash-classes")
                             ;; 3. Set up inbuilt object tables, and
                             ;; 4. Create the metaclass INBUILT-NETFARM-CLASS,
                             ;; which interns without calling a hash function
                             ;; and can have interning deferred.
                             (:file "inbuilt-objects")
                             (:file "external-netfarm-class")
                             ;; 5. Define the class for a schema.
                             (:file "inbuilt-schemas")
                             ;; 6. Allow for class<->schema conversion (though
                             ;; schema->class won't work yet, with no hash
                             ;; function to enable memoisation)
                             ;; 7. Intern the schema class.
                             (:file "schema")
                             (:file "computed-values")
                             (:file "deep-copy")))))
