(asdf:defsystem :netfarm
  :author "Cooperative of Applied Language"
  :license "Cooperative Software License of 2020-11-08+"
  :version "0.0.2"
  :depends-on (:netfarm-early :netfarm-scripts :netfarm-late))
