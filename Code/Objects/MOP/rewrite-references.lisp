(in-package :netfarm)

(defclass rewriting-information-mixin ()
  ((source :initarg :source :initform nil :accessor object-source)))

(defvar *rewrite-references?* t
  "Should we rewrite references?")

(defun overwrite-instance (target source)
  "Overwrite an object TARGET by changing its class to (CLASS-OF SOURCE), and then binding its slots to those of SOURCE."
  (let ((source-class (class-of source)))
    (change-class target source-class)
    (dolist (slot (closer-mop:class-slots source-class))
      (when (closer-mop:slot-boundp-using-class source-class source slot)
        (setf (closer-mop:slot-value-using-class source-class target slot)
              (closer-mop:slot-value-using-class source-class source slot))))
    (initialize-instance target)))

(defun find-schema-in-table (hash table)
  (alexandria:if-let ((class (gethash hash table)))
    (class->schema class)))
(defun reference-replacement (hash function)
  "Find an object with a given hash, possibly calling the function if the object is not known internally."
  (declare (special *classes* *inbuilt-objects* *inbuilt-classes*))
  (or (gethash hash *inbuilt-objects*)
      (find-schema-in-table hash *inbuilt-classes*)
      (find-schema-in-table hash *classes*)
      (funcall function hash)))

(defun rewrite-graph (graph object-source-function)
  (let ((visited (make-hash-table))
        (already-retrieved (make-hash-table :test 'equal))
        (to-visit (acons graph
                         (lambda (new-graph)
                           (setf graph new-graph))
                         '())))
    (loop until (null to-visit)
          do (destructuring-bind (object . writer)
                 (pop to-visit)
               (setf (gethash object visited) t)
               (typecase object
                 (cons
                  (push (cons (car object)
                              (lambda (new-value)
                                (setf (car object) new-value)))
                        to-visit)
                  (push (cons (cdr object)
                              (lambda (new-value)
                                (setf (cdr object) new-value)))
                        to-visit))
                 (reference
                  (assert (stringp (reference-hash object)))
                  (let* ((hash (reference-hash object))
                         (new-object
                           (or (gethash hash already-retrieved)
                               (setf (gethash hash already-retrieved)
                                     (reference-replacement hash object-source-function)))))
                    (funcall writer new-object)))
                 (hash-table
                  (maphash (lambda (key value)
                             (push (cons value
                                         (lambda (new-value)
                                           (setf (gethash key object) new-value)))
                                   to-visit))
                           object)))))
    graph))

(declaim (inline maybe-rewrite-graph))
(defun maybe-rewrite-graph (object graph-cons)
  (when (and (not (car graph-cons))
             *rewrite-references?*)
    (unless (null (object-source object))
      (setf (cdr graph-cons)
            (rewrite-graph (cdr graph-cons) (object-source object))))
    (setf (car graph-cons) t))
  (cdr graph-cons))

;;; Methods to automagically rewrite object graphs in slots to remove references.
(defmethod closer-mop:slot-value-using-class :around
    ((class netfarm-class) object (slot netfarm-effective-slot))
  (maybe-rewrite-graph object (call-next-method)))

(defmethod (setf closer-mop:slot-value-using-class) :around
    (new-value (class netfarm-class) object (slot netfarm-effective-slot))
  (call-next-method (cons nil new-value)
                    class object slot))
