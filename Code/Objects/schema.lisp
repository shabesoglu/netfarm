(in-package :netfarm)

(defgeneric fix-up-copied-object (object)
  (:method ((object object))
    "Set initial values of hidden slots."
    (loop with class = (class-of object)
          for slot in (closer-mop:class-slots class)
          for initfunction = (closer-mop:slot-definition-initfunction slot)
          ;; Fix up any non-Netfarm slots, and any hidden slots.
          when (and (or (not (typep slot 'netfarm-effective-slot))
                        (null (slot-netfarm-name slot)))
                    (not (null initfunction))
                    (not (closer-mop:slot-boundp-using-class class object slot)))
            do (setf (closer-mop:slot-value-using-class class object slot)
                     (funcall initfunction)))))

(defun apply-class (vague-object class)
  "Apply the class CLASS to a VAGUE-OBJECT, matching slot names and types with 
keys in the vague-object's values table."
  (etypecase class
    (class)
    (symbol (setf class (find-class class))))
  (let ((new-object (allocate-instance class))
        (slots (vague-object-values vague-object))
        (computed-slots (vague-object-computed-values vague-object)))
    ;; Initialize signatures and consistent-state stuff. We don't initialize
    ;; the instance normally, so C-S-M slots are undefined.
    (setf (consistent-state-lock new-object)        (bt:make-lock)
          (consistent-state-effect-lock new-object) (bt:make-lock)
          (consistent-state-effects new-object)     '()
          (object-source new-object) (vague-object-source vague-object)
          (object-signatures new-object)
          (loop for (user . signature)
                  in (vague-object-signatures vague-object)
                collect (cons (make-instance 'reference
                                             :hash (bytes->base64 user))
                              signature)))
    (assert (= (length slots) (length (netfarm-class-slot-names class))))
    (assert (= (length computed-slots)
               (length (netfarm-class-computed-slot-names class))))
    (loop for slot-value across slots
          for slot-name across (netfarm-class-slot-names class)
          unless (eql slot-value :unbound)
            do (setf (object-value new-object slot-name)
                     (funcall (slot-parser (find-netfarm-slot class slot-name))
                              slot-value)))
    (loop for values across computed-slots
          for slot-name across (netfarm-class-computed-slot-names class)
          do (setf (object-computed-values new-object slot-name)
                   values))
    (fix-up-copied-object new-object)
    new-object))

(define-condition netfarm-class-not-found (error)
  ((class-name :initarg :class-name
               :reader netfarm-class-not-found-class-name))
  (:report
   (lambda (c s)
     (format s "There is no class named ~s."
             (netfarm-class-not-found-class-name c)))))

(defun find-netfarm-class (name &optional (error? t))
  "Find a Netfarm class with the provided name. If no such class exists and ERROR? is true, a NETFARM-CLASS-NOT-FOUND error will be signalled. If no such class exists and ERROR? is false, NIL will be returned."
  (or (bt:with-lock-held (*hash-lock*)
        (gethash name *classes*))
      (gethash name *inbuilt-classes*)
      (if error?
          (error 'netfarm-class-not-found :class-name name)
          nil)))

;;; Convert schemas to classes and back.

(defun getf/string (name plist &optional default)
  (loop for (key value) on plist by #'cddr
        when (string= name key)
          return (values value t))
  (values default nil))

(defun schema-direct-slots (schema slot-names)
  (append
   (loop for (slot-name . properties) in (schema-slots schema)
         collect `(:name ,(or (cdr (assoc slot-name slot-names :test #'string=))
                              (make-symbol slot-name))
                   :documentation ,(getf/string "documentation" properties)))
   (loop for (slot-name . properties) in (schema-computed-slots schema)
         collect `(:name ,(or (cdr (assoc slot-name slot-names :test #'string=))
                              (make-symbol slot-name))
                   :computed t
                   :documentation ,(getf/string "documentation" properties)))))

(defmacro with-cache ((key table &key (write-back t) lock) &body body)
  (alexandria:once-only (key table)
    (alexandria:with-gensyms (value present?)
      `(multiple-value-bind (,value ,present?)
           ,(if (null lock)
                `(gethash ,key ,table)
                `(bt:with-lock-held (,lock)
                   (gethash ,key ,table)))
         (if ,present?
             ,value
             ,(if write-back
                  `(let ((,value (progn ,@body)))
                     ,(if (null lock)
                          `(setf (gethash ,key ,table) ,value)
                          `(bt:with-lock-held (,lock)
                             (setf (gethash ,key ,table) ,value))))
                  `(progn ,@body)))))))

(defun schema->class (schema &key slot-names)
  "Convert a schema into a NETFARM-CLASS. If a class already exists that corresponds to the schema, it is returned. Otherwise, a new schema is returned. If the association list SLOT-NAMES is provided, each slot name will be looked up in the association list, and the values will be used as slot names instead of creating uninterned symbols."
  (with-cache ((hash-object* schema) *classes* :lock *hash-lock*)
    (make-instance 'external-netfarm-class
     :name (make-symbol (hash-object* schema))
     :direct-slots (schema-direct-slots schema slot-names)
     :documentation (schema-documentation schema)
     :scripts (schema-scripts schema))))

(defun slot-definition->netfarm-slot (name slot-definition)
  (declare (ignore slot-definition))
  `(,name))

(defun netfarm-class-slot-definitions (class)
  (loop for name across (netfarm-class-slot-names class)
        for slot = (find-netfarm-slot class name)
        collect (slot-definition->netfarm-slot name slot)))
(defun netfarm-class-computed-slot-definitions (class)
  (loop for name across (netfarm-class-computed-slot-names class)
        for slot = (find-netfarm-computed-slot class name)
        collect (slot-definition->netfarm-slot name slot)))

(defun make-class-schema (class &rest initargs)
  (apply #'make-instance 'schema
         :computed-slots (netfarm-class-computed-slot-definitions class)
         :documentation (if (typep class 'inbuilt-netfarm-class)
                            ;; As mentioned in "Inbuilt objects" in the Netfarm
                            ;; book, we drop documentation so that scripts
                            ;; cannot depend on it.
                            ""
                            (documentation class 'type))
         :scripts (netfarm-class-scripts class)
         :slots (netfarm-class-slot-definitions class)
         initargs))

(defgeneric %class->schema (class)
  (:method ((class inbuilt-netfarm-class))
    (make-class-schema class :inbuilt-name (netfarm-class-name class)))
  (:method ((class netfarm-class))
    (make-class-schema class)))

(defun class->schema (class)
  "Convert a NETFARM-CLASS into a schema. If a schema for this class already exists, it is returned instead of creating a fresh schema."
  (with-cache (class *schemas*)
    (%class->schema class)))

;; Now that we can create schemas, install the schema of the schema class.
(let ((*intern-hash-of-inbuilt-class?* nil))
  (intern-class (find-class 'schema)))
