(in-package :netfarm)

(defvar *cache-lock* (bt:make-lock "Vector cache locks"))
(defvar *octet-vector-cache*
  (trivial-garbage:make-weak-hash-table :test 'equalp :weakness :value))
(defvar *string-cache*
  (trivial-garbage:make-weak-hash-table :test 'equal :weakness :value))
(defvar *reference-cache*
  (trivial-garbage:make-weak-hash-table :test 'equal :weakness :value))

(defun cached-octet-vector (octet-vector)
  (bt:with-lock-held (*cache-lock*)
    (with-cache (octet-vector *octet-vector-cache*)
      (alexandria:copy-array octet-vector
                             :element-type '(unsigned-byte 8)))))
(defun cached-string (string)
  (bt:with-lock-held (*cache-lock*)
    (with-cache (string *string-cache*)
      (copy-seq string))))
(defun cached-reference (hash)
  (bt:with-lock-held (*cache-lock*)
    (with-cache (hash *reference-cache*)
      (make-instance 'reference :hash (copy-seq hash)))))

(defun normalise-graph (graph)
  "Normalise a graph of objects, so that it will be EQUAL to other graphs like it, and so that strings, references, and octet vectors which are the same will be EQ to each other, and so that objects and vague objects are replaced with references."
  (let ((visited (make-hash-table))
        (to-visit (acons graph
                         (lambda (new-graph)
                           (setf graph new-graph))
                         '())))
    (loop until (null to-visit)
          do (destructuring-bind (object . writer)
                 (pop to-visit)
               (setf (gethash object visited) t)
               (typecase object
                 (cons
                  (let ((new-cons (cons (car object) (cdr object))))
                    (funcall writer new-cons)
                    (push (cons (car object)
                                (lambda (new-value)
                                  (setf (car new-cons) new-value)))
                          to-visit)
                    (push (cons (cdr object)
                                (lambda (new-value)
                                  (setf (cdr new-cons) new-value)))
                          to-visit)))
                 (string
                  (funcall writer (cached-string object)))
                 (vector
                  (funcall writer (cached-octet-vector object)))
                 (reference
                  (funcall writer (cached-reference (reference-hash object))))
                 ((or object vague-object)
                  (funcall writer (cached-reference (hash-object* object)))))))
    graph))
