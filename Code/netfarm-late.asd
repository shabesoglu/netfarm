(asdf:defsystem :netfarm-late
  :depends-on (:alexandria :babel :closer-mop :flexi-streams
               :ironclad :s-base64 :split-sequence
               :trivial-garbage :bordeaux-threads
               :netfarm-early :netfarm-scripts)
  
  :components ((:module "Text-format"
                :depends-on ()
                :serial t
                :components ((:file "base64")
                             (:file "codecs")
                             (:file "new-parser")
                             (:file "new-renderer")))
               (:module "Binary-format"
                :components ((:file "fasterer-output")
                             (:file "parser")
                             (:file "renderer"
                              :depends-on ("fasterer-output"))))
               ;; How to bootstrap Netfarm in 11 not easy steps:
               ;; 9. Make schema->class conversion work by introducing
               ;; hash functions.
               (:module "Crypto"
                :depends-on ("Binary-format")
                :serial t
                :components ((:file "hash")
                             (:file "keys")))
               ;; 10. Now, with correctly set up class and schema graphs,
               ;; proceed with loading inbuilt Netfarm classes.
               (:module "Objects"
                :depends-on ("Crypto")
                :components ((:file "rewrite-values")
                             (:module "Inbuilt-schemas"
                              :serial t
                              :components ((:file "mutable")
                                           (:file "user")
                                           (:file "user-map")
                                           (:file "mirror")
                                           ;; 11. Intern internal schemas by
                                           ;;     hashes too.
                                           (:file "intern-by-hash")))))))
