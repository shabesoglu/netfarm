(asdf:defsystem :netfarm-scripts
  :depends-on (:alexandria :netfarm-early)
  :serial t
  :components ((:file "package")
               ;; How to bootstrap Netfarm in 11 not easy steps:
               ;; 8. Load the script machine.
               (:module "Script-machine"
                :components ((:file "conditions")
                             (:file "opcode-information")
                             (:file "procedures")
                             (:file "scripts")
                             (:file "script-machine")
                             (:file "dispatch-function")
                             (:file "environments")
                             (:file "control-flow")
                             (:file "operators")
                             (:file "forthisms")
                             (:file "objects")
                             (:file "presentation")
                             (:file "assemble")
                             (:file "define-script")))
               (:module "Reference-side-effects"
                :components ((:file "apply-side-effect")
                             (:file "consistent-state")
                             (:file "run-scripts")))
               (:module "Recommenders"
                :components ((:file "protocol")))
               (:file "present")))
