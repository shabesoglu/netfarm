(in-package :netfarm-scripts)

(defvar *default-presentation-script* nil)

(defun compute-presentation-using-method (object)
  (handler-case
      (setup-method-interpreter object "present" '()
                                '(:read-computed-values
                                  :query-recommender))
    (error ()
      (compute-presentation-using-fallback object))
    (:no-error (interpreter)
      (first (run-interpreter interpreter
                              :cycle-limit 100000)))))

(defun compute-presentation-using-fallback (object)
  (when (null *default-presentation-script*)
    (return-from compute-presentation-using-fallback
      `("horizontally"
        ("sequentially"
         "The object " ,object " does not have a method for " ("code" "present") ".")
        ("sequentially"
         "You can bind " ("code" "netfarm-scripts:*default-presentation-script*") " to a fallback script to view it instead."))))
  (first (run-interpreter (setup-interpreter *default-presentation-script*
                                             (list object)
                                             '(:read-computed-values))
                          :cycle-limit 100000)))

(defun compute-presentation (object)
  (handler-case
      (compute-presentation-using-method object)
    (error (e)
      (let ((*print-length* 50)
            (*print-level* 4))
        (list "code"
              (format nil "An error occured when rendering ~s:
  ~a"
                      (netfarm:hash-object* object)
                      e))))))
