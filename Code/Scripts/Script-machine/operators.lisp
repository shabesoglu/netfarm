(in-package :netfarm-scripts)

;;; List processing

(defun kons (first rest)
  "CONS that only makes proper lists"
  (check-type rest list)
  (cons first rest))

(defmacro boolean-wrap (function-name argument-count &key (type t))
  (let ((arguments (loop repeat argument-count collect (gensym "ARGUMENT"))))
    `(lambda ,arguments
       (declare (,type . ,arguments))
       (if (,function-name . ,arguments)
           :true :false))))

(define-function-opcode 64 cons 2)
(define-opcode 65 car (() cons) ()
  (unless (consp cons)
    (error 'type-error
           :context 'cons-to-car
           :expected-type 'cons
           :datum cons))
  (car cons))
(define-opcode 66 cdr (() cons) ()
  (unless (consp cons)
    (error 'type-error
           :context 'cons-to-car
           :expected-type 'cons
           :datum cons))
  (cdr cons))
(define-function-opcode 67 (boolean-wrap null 1)  1 :opcode-name null)
(define-function-opcode 68 (boolean-wrap consp 1) 1 :opcode-name consp)

(define-opcode 69 append ((:instruction-count instruction-count) list1 list2) ()
  (incf instruction-count
        (+ (length list1) (length list2)))
  (append list1 list2))

;;; Generic equal that's slightly less picky than EQUAL because it recurses trees.
;;; It also uses an auxiliary stack, so it won't blow the control stack while
;;; testing deep structures.
(defgeneric netfarm-equal-step (value1 value2)
  (:documentation "Return the values NIL and () if two values differ, or T and some more values to check if they don't immediately look different.")
  (:method (o1 o2)
    (declare (ignore o1 o2))
    (values nil '()))
  (:method :around (o1 o2)
    "Test if the objects are EQ, i.e. if they are physically the same object."
    (if (eq o1 o2)
        (values t '())
        (call-next-method)))
  (:method ((i integer) (j integer)) (values (= i j)         '()))
  (:method ((s1 string) (s2 string)) (values (string= s1 s2) '()))
  (:method ((l1 list) (l2 list))
    (if (= (length l1) (length l2))
        (values t (map 'list #'cons l1 l2))
        (values nil '())))
  (:method ((v1 vector) (v2 vector))
    (if (= (length v1) (length v2))
        (values t (map 'list #'cons v1 v2))
        (values nil '())))
  (:method ((p1 procedure) (p2 procedure))
    (values (and (= (procedure-entry-point p1) (procedure-entry-point p2))
                 (= (procedure-arguments p1) (procedure-arguments p2))
                 (eq (procedure-object p1) (procedure-object p2))
                 (eq (procedure-environment p1) (procedure-environment p2))
                 (eq (procedure-variables p1) (procedure-variables p2))
                 (eq (procedure-program p1) (procedure-program p2)))
            '()))
  (:method ((o1 object) (o2 object))
    (values (string= (hash-object* o1)
                     (hash-object* o2))
            '())))

(defun netfarm-equal (value1 value2)
  "Test if VALUE1 and VALUE2 are structurally equal. 
One can imagine that if two objects render to the same bytes or characters, then they are NETFARM-EQUAL."
  (when (eql value1 value2)
    ;; Avoid setting up anything if possible.
    (return-from netfarm-equal t))
  (let ((stack (acons value1 value2 '())))
    (loop
      (when (null stack)
        (return-from netfarm-equal t))
      (destructuring-bind (v1 . v2) (pop stack)
        (multiple-value-bind (continue? more)
            (netfarm-equal-step v1 v2)
          (unless continue?
            (return-from netfarm-equal nil))
          (setf stack (append more stack)))))))

(define-function-opcode 71 (boolean-wrap netfarm-equal 2) 2 :opcode-name equal)
(define-function-opcode 72 (boolean-wrap string= 2) 2 :opcode-name string=)


(define-opcode 73 list ((:instruction-count instruction-count
                         :data-stack data-stack))
    (count)
  (incf instruction-count count)
  (reverse
   (loop repeat count
         collect (pop* data-stack))))

;;; Number processing

(defmacro check-integer-in-range (n)
  (let ((error-form
          `(error 'overflow
                  :operands (list ,@(rest n))
                  :operation ',(first n))))
    (alexandria:once-only (n)
      ;; If fixnums are smaller than 256 bits, then FIXNUM-ness implies that
      ;; the integer is in range.
      `(progn
         (unless (or ,(if (or (typep (1- (expt 2 255)) 'fixnum)
                              (typep (- (expt 2 255)) 'fixnum))
                          'nil
                          `(typep ,n 'fixnum))
                     (<= (- (expt 2 255)) ,n (1- (expt 2 255))))
           ,error-form)
         ,n))))

(defmacro define-integer-function-opcode
    (number function argument-count &key (opcode-name function))
  (let ((args (loop repeat argument-count collect (gensym "ARGUMENT"))))
    `(define-function-opcode ,number (lambda ,args (,function ,@args))
       ,argument-count :opcode-name ,opcode-name)))

(define-integer-function-opcode 96  + 2)
(define-integer-function-opcode 97  - 2)
(define-integer-function-opcode 98  * 2)
(define-integer-function-opcode 99  floor 2 :opcode-name /)
(define-integer-function-opcode 100 abs 1)

(define-opcode 101 string-length (() string) ()
  (check-type string string)
  (length string))
(define-opcode 102 string-ref (() string position) ()
  (check-type string string)
  (string (char string position)))

;;; 103: (string-append) strings -- reduce(append, strings)
(define-opcode 103 string-append ((:instruction-count instruction-count)
                                  strings)
    ()
  (let ((total-length 0))
    (dolist (string strings)
      (assert (stringp string))
      (incf total-length (length string)))
    (incf instruction-count total-length)
    (loop with new-string = (make-string total-length)
          for string in strings
          and position = 0 then (+ position (length string))
          do (replace new-string string :start1 position)
          finally (return new-string))))

(define-function-opcode 105 (boolean-wrap integerp 1) 1 :opcode-name integerp)
(define-function-opcode 106 (boolean-wrap listp 1)    1 :opcode-name listp)
(define-function-opcode 108 (boolean-wrap stringp 1)  1 :opcode-name stringp)

(define-function-opcode 112 (boolean-wrap = 2 :type integer)  2 :opcode-name =)
(define-function-opcode 113 (boolean-wrap < 2 :type integer)  2 :opcode-name <)
(define-function-opcode 114 (boolean-wrap > 2 :type integer)  2 :opcode-name >)
(define-function-opcode 115 (boolean-wrap /= 2 :type integer) 2 :opcode-name /=)
(define-function-opcode 116 (boolean-wrap <= 2 :type integer) 2 :opcode-name <=)
(define-function-opcode 117 (boolean-wrap >= 2 :type integer) 2 :opcode-name >=)
