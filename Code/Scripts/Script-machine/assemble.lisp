(in-package :netfarm-scripts)

(defun get-entry-points (code)
  (loop with position = 0
        for item in code
        for (name . args) = (alexandria:ensure-list item)
        if (eql name :procedure)
          collect (cons position args)
        else
          do (incf position (1+ (length args)))))

(defun get-methods (code)
  (loop with position = 0
        for item in code
        for (name . args) = (alexandria:ensure-list item)
        if (eql name :method)
          collect (destructuring-bind (name argument-count) args
                    (list name position argument-count))
        else
          do (incf position (1+ (length args)))))

(defun assemble-instruction (instruction)
  (let ((instruction (alexandria:ensure-list instruction)))
    (if (keywordp (car instruction))
        '()
        (let ((opcode (position (car instruction) *opcode-data*
                                :key #'car :test #'string-equal)))
          (assert (not (null opcode)) () "Unknown opcode ~a" (car instruction))
          (let ((argument-count (length (third (aref *opcode-data* opcode)))))
            (assert (= (length (rest instruction)) argument-count) ()
                    "Wanted ~d argument~:p for ~a, got ~d"
                    argument-count
                    (first instruction)
                    (length (third instruction)))
            (cons opcode (cdr instruction)))))))
          

(defun assemble (code)
  (let ((program
          (coerce 
           (loop for instruction in code
                 append (assemble-instruction instruction))
           '(vector (unsigned-byte 8)))))
    (values (get-entry-points code)
            (get-methods code)
            program)))
