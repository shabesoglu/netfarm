(in-package :netfarm-scripts)

;;; This file implements another type of dispatch for the bytecode interpreter,
;;; which is similar to what is described in "Fast generic dispatch for Common
;;; Lisp" by Robert Strandh <http://metamodular.com/SICL/generic-dispatch.pdf>

;;; This dispatch is expected to be faster on machines with slower memory than
;;; registers; as it replaces one table lookup with binary search that can be
;;; performed entirely in a register set. It also avoids a couple of function
;;; calls and memory accesses per cycle, which yields very impressive performance
;;; gains. 

(defvar *dispatch-function* nil)
(defun ensure-dispatch-function ()
  (when (null *dispatch-function*)
    (write-line ";;; Compiling the dispatch function..." *debug-io*)
    (setf *dispatch-function* (make-dispatch-function))))

(defmacro instruction-body (number &body body)
  "A useless macro that makes the dispatch function code slightly more readable."
  (declare (ignore number))
  `(progn ,@body))

(defmacro with-interpreter-parts-renamed ((&rest parts) &body body)
  `(symbol-macrolet ,(loop for (name variable) on parts by #'cddr
                           for new-name = (intern (string name) :netfarm-scripts)
                           unless (eql variable new-name)
                             collect `(,variable ,new-name))
     ,@body))

(defun make-dispatch-parts (opcodes)
  (cond
    ((null opcodes)
     (error "no opcodes to dispatch on?"))
    ((alexandria:length= 1 opcodes)
     (destructuring-bind (opcode) opcodes
       (if (null (aref *opcode-code* opcode))
           `(go lose)
           (destructuring-bind ((parts inputs bytes) declarations body)
               (aref *opcode-code* opcode)
             `(instruction-body ,opcode
                (with-interpreter-parts-renamed (data-stack %%data-stack
                                                 program-counter %%program-counter
                                                 program %%program
                                                 . ,parts)
                  (let (,@(loop for input in (reverse inputs)
                                collect #+sbcl `(,input
                                                 (sb-ext:truly-the interpreter-datum
                                                                   (pop* %%data-stack)))
                                #-sbcl `(,input (pop* %%data-stack)))
                        ,@(loop for byte in bytes
                                for position from 0
                                collect `(,byte (aref %%program
                                                      (+ ,position %%program-counter)))))
                    (declare (ignorable ,@inputs ,@bytes))
                    ,@declarations
                    (incf %%program-counter ,(length bytes))
                    ,body
                    (go out))))))))
    ((every (lambda (opcode)
              (null (aref *opcode-code* opcode)))
            opcodes)
     '(go lose))
    (t
     (let* ((middle (floor (length opcodes) 2))
            (middle-element (nth middle opcodes)))
       `(if (< opcode ,middle-element)
            ,(make-dispatch-parts (subseq opcodes 0 middle))
            ,(make-dispatch-parts (subseq opcodes middle)))))))

(defun make-dispatch-code ()
  (labels ((in-bounds? (opcode)
             (<= 0 opcode 255))
           (exists? (opcode)
             (and (in-bounds? opcode)
                  (not (null (aref *opcode-code* opcode)))))
           (needed? (opcode)
             (or (exists? opcode)
                 (exists? (1+ opcode))
                 (exists? (1- opcode)))))
    (make-dispatch-parts
     (loop for opcode below 256
           when (needed? opcode)
             collect opcode))))

(defmacro let-interpreter-parts ((&rest names) (&rest read-only-names) &body body)
  "Bind interpreter registers to local variables, and then set them back when we leave the lexical extent of this form."
  `(let ,(loop for name in names
               collect `(,name (,(alexandria:format-symbol :netfarm-scripts
                                                           "INTERPRETER-~a"
                                                           name)
                                interpreter)))
     (unwind-protect
          (locally ,@body)
       (setf ,@(loop for name in names
                     unless (member name read-only-names)
                     appending `((,(alexandria:format-symbol :netfarm-scripts
                                                             "INTERPRETER-~a"
                                                             name)
                                  interpreter)
                                 ,name))))))

(defun make-dispatch-function ()
  (compile nil
           `(lambda (interpreter cycle-limit)
              (declare (optimize (speed 3) (safety 1)
                                 (debug 0))
                       (interpreter interpreter)
                       ((or null fixnum) cycle-limit)
                       #+sbcl (sb-ext:muffle-conditions sb-ext:compiler-note))
              (let-interpreter-parts (program-counter variables program
                                      call-stack data-stack environment
                                      current-object last-object
                                      entry-points side-effects capabilities
                                      instruction-count)
                  (capabilities)
                (declare (reasonably-sized-natural program-counter
                                                   instruction-count)
                         (list environment side-effects capabilities
                               call-stack data-stack)
                         ((or null object) current-object last-object)
                         ((simple-array (unsigned-byte 8) (*)) program)
                         (simple-vector variables)
                         ((simple-array procedure-description (*)) entry-points))
                (loop
                  (when (>= program-counter (length program))
                    (error 'program-counter-out-of-bounds
                           :program-counter program-counter))
                  (let ((opcode (aref program program-counter)))
                    (declare ((unsigned-byte 8) opcode))
                    (incf instruction-count)
                    (incf program-counter)
                    (tagbody
                       ,(make-dispatch-code)
                     lose
                       (error 'invalid-instruction
                              :instruction opcode
                              :program-counter program-counter)
                     out
                       (when (and (not (null cycle-limit))
                                  (> instruction-count cycle-limit))
                         (error 'exceeded-cycle-limit)))))))))
