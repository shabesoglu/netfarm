(in-package :netfarm-scripts)

;;; Global state manipulation
;;; 01: (get-proc n) -- proc
(define-opcode 1 get-proc ((:variables variables :program program
                            :entry-points entry-points :environment environment
                            :current-object object))
    (n)
  (let ((description (aref entry-points n)))
    (make-procedure :entry-point (procedure-description-entry-point description)
                    :entry-points entry-points
                    :arguments (procedure-description-arguments description)
                    :program program
                    :variables variables
                    :environment environment
                    :object object)))

;;; 06: (get-proc* n) -- proc
;;; Doesn't capture the lexical environment. Technically, top-level procedures
;;; will never access any lexical environment other than their arguments, but they
;;; still consume space. get-proc* in a way is to get-proc as tail-call is to call.
(define-opcode 6 get-proc* ((:variables variables :program program
                             :entry-points entry-points :environment environment
                             :current-object object))
    (n)
  (let ((description (aref entry-points n)))
    (make-procedure :entry-point (procedure-description-entry-point description)
                    :entry-points entry-points
                    :arguments (procedure-description-arguments description)
                    :program program
                    :variables variables
                    :environment '()
                    :object object)))

;;; 02: (get-value n) -- value
(define-opcode 2 get-value ((:variables variables)) (n)
  (aref variables n))
;;; 03: (set-value! n) new-value --
(define-opcode* 3 set-value! ((:variables variables) new-value) (n)
  (setf (aref variables n) new-value))

(defmacro truly-the (type value)
  (declare (ignorable type))
  #+sbcl
  `(sb-ext:truly-the ,type ,value)
  ;; No point adding the hint then.
  #-sbcl value)

;;; Environments
;;; 04: (get-env variable frame) -- value
(define-opcode 4 get-env ((:environment environment))
    (variable-position frame-position)
  (let ((frame (truly-the (or null simple-vector)
                          (nth frame-position environment))))
    (when (null frame)
      (error "No frame ~d" frame-position))
    (aref frame variable-position)))
;;; 05: (set-env! variable frame) new-value --
(define-opcode* 5 set-env! ((:environment environment) new-value)
    (variable-position frame-position)
  (let ((frame (truly-the (or null simple-vector)
                          (nth frame-position environment))))
    (when (null frame)
      (error "No frame ~d" frame-position))
    (setf (aref frame variable-position) new-value)))

;;; 07: self -- self
(define-opcode 7 self ((:current-object object)) () object)
;;; 0C: sender -- sender
(define-opcode 12 sender ((:last-object object)) () object)

;;; 0B: (byte x) -- x
(define-opcode 11 byte (()) (byte) byte)
