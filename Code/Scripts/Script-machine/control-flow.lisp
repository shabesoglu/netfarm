(in-package :netfarm-scripts)

(declaim (inline make-frame))
(defun make-frame (interpreter frame-size)
  (let ((frame (make-array frame-size)))
    (loop for n from (1- frame-size) downto 0
          when (null (interpreter-data-stack interpreter))
            do (error "out of data when creating frame")
          do (setf (aref frame n)
                   (pop (interpreter-data-stack interpreter))))
    frame))

;;; A call frame is of the form #(method? environment program-counter program entry-points variables current-object last-object data-stack side-effects
(defmacro save-context ((method? environment program-counter
                         program entry-points variables
                         current-object last-object
                         data-stack side-effects)
                        call-stack)
  `(push (vector ,method? ,environment ,program-counter
                 ,program ,entry-points ,variables
                 ,current-object ,last-object
                 ,data-stack ,side-effects)
         ,call-stack))
(defmacro restore-context ((environment program-counter
                            program entry-points variables
                            current-object last-object
                            data-stack side-effects)
                           value frame)
  (alexandria:once-only (frame)
     `(locally
          (declare (optimize (safety 0) (speed 3))
                   ((simple-vector 10) ,frame))
        (setf ,environment (svref ,frame 1)
              ,program-counter (svref ,frame 2)
              ,program (svref ,frame 3)
              ,entry-points (svref ,frame 4)
              ,variables (svref ,frame 5)
              ,current-object (svref ,frame 6)
              ,last-object (svref ,frame 7)
              ,data-stack (cond
                            ((svref ,frame 0) ; Have we just left a method?
                             (cons (list ,value) (svref ,frame 8)))
                            (t
                             (cons ,value (svref ,frame 8))))
              ,side-effects ,side-effects))))
(defmacro enter-function ((environment program-counter
                           program entry-points
                           data-stack
                           variables object)
                          arguments function)
  (alexandria:once-only (function)
    `(setf ,environment     (cons ,arguments (procedure-environment ,function))
           ,program         (procedure-program ,function)
           ,program-counter (procedure-entry-point ,function)
           ,entry-points    (procedure-entry-points ,function)
           ,variables       (procedure-variables ,function)
           ,object          (procedure-object ,function)
           ,data-stack      '())))
(defmacro with-unwinding ((environment program-counter
                           program entry-points variables
                           current-object last-object
                           data-stack call-stack side-effects)
                          &body body)
  "Unwind the stack past the last method call, push #f on the data stack, and evaluate BODY."
  (alexandria:with-gensyms (frame)
    `(loop until (null ,call-stack)
           do (let ((,frame (pop ,call-stack)))
                (locally
                    (declare (optimize (safety 0) (speed 3))
                             ((simple-vector 10) ,frame))
                  (when (svref ,frame 0)
                    (setf ,environment (svref ,frame 1)
                          ,program-counter (svref ,frame 2)
                          ,program (svref ,frame 3)
                          ,entry-points (svref ,frame 4)
                          ,variables (svref ,frame 5)
                          ,current-object (svref ,frame 6)
                          ,last-object (svref ,frame 7)
                          ,data-stack (cons :false (svref ,frame 8))
                          ,side-effects (svref ,frame 9))
                    ,@body))))))
(defun unwind-interpreter (interpreter continuation)
  (with-unwinding ((interpreter-environment interpreter)
                   (interpreter-program-counter interpreter)
                   (interpreter-program interpreter)
                   (interpreter-entry-points interpreter)
                   (interpreter-variables interpreter)
                   (interpreter-current-object interpreter)
                   (interpreter-last-object interpreter)
                   (interpreter-data-stack interpreter)
                   (interpreter-call-stack interpreter)
                   (interpreter-side-effects interpreter))
    (funcall continuation)))

(defun call-function (interpreter function arguments &key (save-current t))
  (setf (interpreter-data-stack interpreter)
        (append (cons function (reverse arguments))
                (interpreter-data-stack interpreter))
        ;; Install an argument count.
        (interpreter-program-counter interpreter) 0
        (interpreter-program interpreter)
        (make-array 1
                    :element-type '(unsigned-byte 8)
                    :initial-element (length arguments)))
  (let ((opcode (if save-current 9 10)))
    (funcall (aref *operators* opcode)
             interpreter opcode))
  t)

(defun call-object-method (interpreter object method-name arguments)
  (setf (interpreter-data-stack interpreter)
        (append (list* method-name object (reverse arguments))
                (interpreter-data-stack interpreter))
        ;; Install an argument count.
        (interpreter-program-counter interpreter) 0
        (interpreter-program interpreter)
        (make-array 1
                    :element-type '(unsigned-byte 8)
                    :initial-element (length arguments)))
  (funcall (aref *operators* 19)
           interpreter 19)
  t)

;;; Control flow
;;; 08: (return) --
(define-opcode* 8 return ((:call-stack call-stack :data-stack data-stack
                           :environment environment :entry-points entry-points
                           :program program :variables variables
                           :current-object current-object :last-object last-object
                           :program-counter program-counter
                           :side-effects side-effects)
                          value)
    ()
  (when (null call-stack)
    (push value data-stack)
    ;; Not really an error, but if it isn't handled (somehow), we can't do
    ;; anything useful.
    (error 'done))
  (let ((last-frame (pop call-stack)))
    (restore-context (environment program-counter
                      program entry-points variables
                      current-object last-object
                      data-stack side-effects)
                     value last-frame)))

;;; 09: (call n) arg-1 ... arg-n function --
(define-opcode* 9 call ((:data-stack data-stack
                         :call-stack call-stack :entry-points entry-points
                         :current-object object :last-object last-object
                         :program program :variables variables
                         :program-counter program-counter
                         :environment environment
                         :side-effects side-effects)
                        function)
    (argument-count)
  (check-type function procedure)
  (assert (= argument-count (procedure-arguments function)) ()
          "wanted ~d argument~:p but got ~d"
          (procedure-arguments function) argument-count)
  (let ((arguments (make-array argument-count)))
    ;; Arguments are given "in reverse" from the stack's point of view; eg
    ;; (byte 1) (byte 2) (byte 3) (get-proc 0) call
    ;; is ((get-proc 0) 1 2 3), which is the right way around,  but the stack
    ;; just before GET-PROC will contain
    ;; 3 2 1
    (loop for n from (1- argument-count) downto 0
          when (null data-stack)
            do (error "out of data when creating frame")
          do (setf (svref arguments n)
                   (pop data-stack)))
    (save-context (nil
                   environment program-counter program entry-points
                   variables object last-object data-stack
                   side-effects)
                  call-stack)
    (enter-function (environment program-counter program
                     entry-points data-stack variables object)
                    arguments function)))
  
;;; 0A: (tail-call n) arg-1 ... arg-n function --
;;; Similar to CALL, but doesn't back up the last state on the stack. This is
;;; therefore only good for tail-calls. It's really good at those though.
(define-opcode* 10 tail-call ((:data-stack data-stack
                               :call-stack call-stack
                               :current-object object :entry-points entry-points
                               :program program :variables variables
                               :program-counter program-counter
                               :environment environment
                               :side-effects side-effects)
                              function)
    (argument-count)
  (pop environment)
  (check-type function procedure)
  (assert (= argument-count (procedure-arguments function)) ()
          "wanted ~d argument~:p but got ~d"
          (procedure-arguments function) argument-count)
  (let ((arguments (make-array argument-count)))
    (loop for n from (1- argument-count) downto 0
          when (null data-stack)
            do (error "out of data when creating frame")
          do (setf (svref arguments n)
                   (pop data-stack)))
    (enter-function (environment program-counter program
                     entry-points data-stack variables object)
                    arguments function)))

;;; 13: (call-method n) arg-1 .. arg-n object method-name --
(define-opcode* 19 call-method ((:data-stack data-stack
                                 :call-stack call-stack
                                 :current-object current-object :last-object last-object
                                 :program program :variables variables
                                 :entry-points entry-points
                                 :program-counter program-counter
                                 :environment environment
                                 :side-effects side-effects)
                                object method-name)
    (argument-count)
  (check-type method-name string)
  ;; Objects can't call "initialize".
  (when (and (string= method-name "initialize")
             (not (null current-object)))
    (push "error" data-stack)
    (return-from call-method))
  ;; The implicit first argument to a method procedure is the list of the rest
  ;; of the methods, so our frame is one larger than usual.
  (let ((arguments (make-array (1+ argument-count))))
    (loop for n from argument-count downto 1
          when (null data-stack)
            do (error "out of data when creating frame")
          do (setf (svref arguments n)
                   (pop data-stack)))
    (multiple-value-bind (strategy methods)
        (call-method-strategy object method-name)
      (when (eql strategy :no-methods)
        ;; We can't call any method.
        (dotimes (i argument-count)
          (pop data-stack))
        (push "error" data-stack)
        (return-from call-method))
      ;; Bind the next methods.
      (let ((method (first methods))
            frame)
        (case strategy
          (:methods
           ;; We have at least one method, so call it.
           (setf method (first methods)
                 frame  arguments)
           (assert (= argument-count (1- (procedure-arguments method))) ()
                   "wanted ~d argument~:p but got ~d"
                   (1- (procedure-arguments method)) argument-count))
          (:does-not-understand
           ;; We have methods for does-not-understand.
           ;; The frame should be (next-methods method-name arguments)
           (setf frame (vector (rest methods) method-name
                               (coerce (subseq arguments 1) 'list)))
           (assert (= 3 (procedure-arguments method)) ()
                   "wanted ~d argument~:p but got ~d"
                   (procedure-arguments method) 2)))
        (setf (svref frame 0) (rest methods))
        (save-context (t
                       environment program-counter program entry-points
                       variables current-object last-object data-stack
                       side-effects)
                      call-stack)
        (setf last-object current-object)
        (enter-function (environment program-counter program
                         entry-points data-stack variables current-object)
                        frame method)))))

;;; 14: (apply-method n) object method-name arguments --
;; The observant reader will notice that we can APPLY methods, but not
;; functions. This asymmetry is supposed to reflect the asymmetry in how we
;; handle procedures; "internal" procedures have fixed arity, whereas method
;; arity is only fixed if the programmer decides it should be.
;; APPLYing methods also makes does-not-understand significantly more useful,
;; as it can be used to "proxy" messages to another object.
(define-opcode* 20 apply-method ((:data-stack data-stack
                                  :call-stack call-stack
                                  :current-object current-object :last-object last-object
                                  :program program :variables variables
                                  :entry-points entry-points
                                  :program-counter program-counter
                                  :environment environment
                                  :side-effects side-effects)
                                 object method-name arguments)
    ()
  (check-type method-name string)
  (check-type arguments list)
  ;; Objects can't call "initialize".
  (when (and (string= method-name "initialize")
             (not (null current-object)))
    (push :false data-stack)
    (return-from apply-method))
  ;; The implicit first argument to a method procedure is the list of the rest
  ;; of the methods, so our frame is one larger than usual.
  (let ((frame (make-array (1+ (length arguments)))))
    (replace frame arguments :start1 1)
    (multiple-value-bind (strategy methods)
        (call-method-strategy object method-name)
      (when (eql strategy :no-methods)
        (push :false data-stack)
        (return-from apply-method))
      ;; Bind the next methods.
      (let ((method (first methods)))
        (case strategy
          (:methods
           ;; We have at least one method, so call it.
           (setf method          (first methods)
                 (svref frame 0) (rest methods))
           (let ((argument-count (length arguments)))
             (assert (= (1+ argument-count) (procedure-arguments method)) ()
                     "wanted ~d argument~:p but got ~d"
                     (procedure-arguments method) (1+ argument-count))))
          (:does-not-understand
           ;; We have methods for does-not-understand.
           (setf frame (vector (rest methods) method-name arguments))
           (assert (= 3 (procedure-arguments method)) ()
                   "wanted ~d argument~:p but got ~d"
                   (procedure-arguments method))))
        (save-context (t
                       environment program-counter program entry-points
                       variables current-object last-object data-stack
                       side-effects)
                      call-stack)
        (setf last-object current-object)
        (enter-function (environment program-counter program
                         entry-points data-stack variables current-object)
                        frame method)))))

(declaim (inline u8-pair->u16))
(defun u8-pair->u16 (high low)
  (logior (ash high 8)
          low))

;;; 10: (cond-jump then-high then-low else-high else-low) test-value --
;;; Let then = then-high * 256 + then-low,
;;;     else = else-high * 256 + else-low
;;; Jump forwards `then` bytes if test-value is true, else jump forwards `else` bytes.
(define-opcode* 16 jump-cond ((:program-counter program-counter) test-value)
    (then-high then-low else-high else-low)
  (let ((then (u8-pair->u16 then-high then-low))
        (else (u8-pair->u16 else-high else-low)))
    (if (eql test-value :false)
        (incf program-counter else)
        (incf program-counter then))))

(define-opcode* 17 jump ((:program-counter program-counter))
    (high low)
  (incf program-counter (u8-pair->u16 high low)))

;;; 18: (error) message cause --
(define-opcode* 24 error (() message cause) ()
  (error 'script-machine-signalled-error :message message :cause cause))
