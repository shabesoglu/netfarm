(in-package :netfarm-scripts)

(defvar *computed-value-deficits*
  (trivial-garbage:make-weak-hash-table :test 'eq
                                        :weakness :key)
  "A hash table mapping target to a table mapping (name value) to the number of computed values we are in 'debt' of.
This allows us to issue REMOVE-COMPUTED-VALUE and then ADD-COMPUTED-VALUE and have the former cancel out the latter, without putting anything in the object.")
(defun computed-value-deficit (target name value)
  (multiple-value-bind (table present?)
      (gethash target *computed-value-deficits*)
    (if present?
        (gethash (list name value) table 0)
        0)))
(defun (setf computed-value-deficit) (new-deficit target name value)
  (let ((key (list name value)))
    (multiple-value-bind (table present?)
        (gethash target *computed-value-deficits*)
      (unless present?
        (setf table (make-hash-table :test 'eql)
              (gethash target *computed-value-deficits*) table))
      (setf (gethash key table) new-deficit))))

(defgeneric %apply-side-effect (target type &key &allow-other-keys)
  (:method (target (type (eql 'add-computed-value)) &key slot value)
    (let ((normalised-value (netfarm:normalise-graph value))
          (name (netfarm:slot-netfarm-name slot)))
      (cond
        ((plusp (computed-value-deficit target name normalised-value))
         (decf (computed-value-deficit target name normalised-value)))
        (t
         (push value (object-computed-values target name))))))
  (:method (target (type (eql 'remove-computed-value)) &key slot value)
    (let ((normalised-value (netfarm:normalise-graph value))
          (name (netfarm:slot-netfarm-name slot)))
      (cond
        ((plusp (computed-value-deficit target name normalised-value))
         ;; If we have a deficit, we shouldn't have any computed values.
         (incf (computed-value-deficit target name normalised-value)))
        ((find value (object-computed-values target name) :test #'equal)
         (setf (object-computed-values target name)
               (remove normalised-value (object-computed-values target name)
                       :key #'netfarm:normalise-graph
                       :test #'equal
                       :count 1)))
        (t (incf (computed-value-deficit target name normalised-value))))))
  (:method (target (type (eql 'set-counters)) &key counters)
    "Set the counters to values provided by a Netfarm node.
Note that this effect is not commutative, and generally it will never be produced, except by a client that needs to 'reset' computed values."
    ;; Clear out old computed values.
    (remhash target *computed-value-deficits*)
    (let ((slot-names (netfarm:netfarm-class-computed-slot-names
                       (class-of target))))
      (loop for name across slot-names
            do (setf (object-computed-values target name) '()))
      ;; Install new computed values and deficits according to counters.
      (loop for (position value count) in counters
            for slot-name = (aref slot-names position)
            if (minusp count)
              do (setf (computed-value-deficit target slot-name value)
                       (abs count))
            else
              do (alexandria:appendf (object-computed-values target slot-name)
                                     (make-list count :initial-element value))))))
